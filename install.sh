#!/usr/bin/env sh

[ "$(whoami)" = "root" ] && echo "This is only built for user installations. Re-run as non-root user." && exit 1

BIN_DIR="$HOME/.local/bin"
APP_DIR="$HOME/.local/share/applications"

for i in $(basename -s '.png' $(ls icons)); do
  echo -n "Installing icon, size $i×$i... "
  xdg-icon-resource install --size "$i" "icons/$i.png" nintendo-switch \
    && echo "Icon installed!" \
    || echo "Failed to install icon."
done

echo -n "Installing launch script... "
mkdir -p $BIN_DIR \
  && cp play-switch "$BIN_DIR/play-switch" \
  && echo "Launcher installed!" \
  || echo "Failed to install launch script."

echo -n "Installing desktop entry... "
xdg-desktop-menu install play-switch.desktop \
  && echo "Desktop entry installed!" \
  || echo "Failed to install desktop entry."
